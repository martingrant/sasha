//
//  Node.h
//  sasha
//
//  Created by Martin Grant on 16/03/2019.
//  Copyright © 2019 Martin Grant. All rights reserved.
//

#ifndef Node_h
#define Node_h

template <class T>
class Node {
public:
    Node(T val) { value = val; }
    T value;
    Node* next = nullptr;
};

#endif /* Node_h */
