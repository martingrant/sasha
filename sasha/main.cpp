//
//  main.cpp
//  sasha
//
//  Created by Martin Grant on 15/03/2019.
//  Copyright © 2019 Martin Grant. All rights reserved.
//

#include <iostream>
#include <string>

#include "LinkedList.h"

int main(int argc, const char * argv[]) {
    std::cout << "Hello, World!\n";
    
    LinkedList<int>* ll = new LinkedList<int>();
    
    ll->add(5);
    ll->add(2);
    ll->add(14);
    
    ll->print();
    
    LinkedList<std::string>* ll2 = new LinkedList<std::string>();
    ll2->add("hello");
    ll2->add("world");
    ll2->add("test");
    
    ll2->print();
    
    return 0;
}
