//
//  LinkedList.h
//  sasha
//
//  Created by Martin Grant on 16/03/2019.
//  Copyright © 2019 Martin Grant. All rights reserved.
//

#ifndef LinkedList_h
#define LinkedList_h

#include "Node.h"

template <class T>
class LinkedList {
public:
    LinkedList() {
        
    }
    ~LinkedList() {
        Node<T>* temp = head;
        
        while (temp->next != nullptr) {
            temp = temp->next;
        }
        
        
    }
    
    void add(T value) {
        Node<T>* newNode = new Node<T>(value);
        
        if (head == nullptr) {
            head = newNode;
        } else {
            Node<T>* temp = head;
            
            while (temp->next != nullptr) {
                temp = temp->next;
            }
            
            temp->next = newNode;
        }
    }
    
    
    void print() {
        if (head != nullptr) {
            Node<T>* temp = head;
            while (temp != nullptr) {
                std::cout << temp->value << std::endl;
                temp = temp->next;
            }
        }
    }
    
private:
    Node<T>* head;
};

#endif /* LinkedList_h */
